class VisitorsController < ApplicationController
  def index
    @posts = Post.limit(10).order(created_at: :asc)
  end
end

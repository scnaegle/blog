class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit

  protect_from_forgery with: :exception
  before_filter :set_current_user


  def set_current_user
    User.current = current_user
  end

  def pundit_user
    current_user
  end
end

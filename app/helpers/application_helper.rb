module ApplicationHelper
  def link_to_edit(url, options={}, &block)
    content = content_tag(:span, "", class: "glyphicon glyphicon-edit")
    content += capture(&block) if block_given?
    link_to content, url, options 
  end

  def link_to_destroy(url, options={}, &block)
    last_obj = url
    last_obj = url.last if url.is_a? Array
    options = { method: :delete, data: { confirm: "Are you sure you want to annihilate this #{last_obj.class.name.humanize.downcase}? Once annihilated, by definition it cannot be reconstituted." } }.merge(options)
    content = content_tag(:span, "", class: "glyphicon glyphicon-fire")
    content += capture(&block) if block_given?
    link_to content, url, options
  end
end

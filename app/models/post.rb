class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :user_id, presence: true
  validates :title, presence: true
  validates :body, presence: true

  before_validation :set_current_user, on: :create

  private
    
    def set_current_user
      self.user_id = User.current.id
    end
end

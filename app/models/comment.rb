class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :post

  validates :user_id, presence: true
  validates :post_id, presence: true
  validates :body, presence: true

  before_validation :set_current_user, on: :create

  private
    
    def set_current_user
      self.user_id = User.current.id
    end
end
